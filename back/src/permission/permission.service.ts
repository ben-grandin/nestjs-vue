import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Permission } from './permission.entity';
import { AbstractService } from '../common/abstract/abstract.service';

@Injectable()
export class PermissionService extends AbstractService<Permission> {
	constructor(@InjectRepository(Permission) private readonly roleRepository: Repository<Permission>) {
		super(roleRepository);
	}
}
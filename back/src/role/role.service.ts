import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Role } from './role.entity';
import { Repository } from 'typeorm';
import { AbstractService } from '../common/abstract/abstract.service';

@Injectable()
export class RoleService extends AbstractService<Role> {
	constructor(@InjectRepository(Role) private readonly roleRepository: Repository<Role>) {
		super(roleRepository);
	}
}
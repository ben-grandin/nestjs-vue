import { Injectable } from '@nestjs/common';
import { AbstractService } from '../common/abstract/abstract.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Order } from './order.entity';
import { Repository } from 'typeorm';

@Injectable()
export class OrderService extends AbstractService<Order> {
	constructor(
		@InjectRepository(Order) private readonly orderRepository: Repository<Order>,
	) {
		super(orderRepository);
	}


	async paginate(page = 1, relations = []): Promise<any> {
		const { data, meta } = await super.paginate(page, relations);

		return {
			data: data.map(order => {
				console.log(order);
				order = {
					...order,
					name: order.name,
					total: order.total,
				};

				delete order.first_name;
				delete order.last_name;

				return order;
			}),
			meta,
		};
	}

	async chart() {
		return await this.orderRepository.query('SELECT DATE_FORMAT(o.created_at, \'%Y-%m-%d\') as date, sum(oi.price * oi.price) as sum FROM orders as o JOIN order_items oi on o.id = oi.order_id GROUP BY date');
	}
}
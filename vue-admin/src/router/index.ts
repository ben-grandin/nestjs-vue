import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import Wrapper from '@/pages/Wrapper.vue'
import Dashboard from '@/pages/Dashboard.vue'
import Register from '@/pages/Register.vue'

const routes: Array<RouteRecordRaw> = [

  {
    path: '',
    component: Wrapper,
    children: [
      { path: '', component: Dashboard },
      { path: '/users', component: () => import(/* webpackChunkName: "users" */ '@/pages/Users.vue') },

    ],
  },
  { path: '/register', component: Register },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router
